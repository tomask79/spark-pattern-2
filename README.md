# Hortonworks Spark Certification test pattern, question 2. #

Second task from the exam pattern is:     

1st task is to extract the data from csv files from hdfs, apply filters and ordering with specified columns.

**2nd task is similar as 1st but to retrieve the data from json.**

Alright, let's go through the all steps. Let's have following JSON file saved at HDFS under /tests/json.

    [root@sandbox ~]# cat people.json 
    {"name": "John", "age": "28", "country": "UK"}
    {"name": "Cathy", "age": "30", "country": "AUS"}
    {"name": "Mark", "age": "50", "country": "USA"}
    [root@sandbox ~]# 

## Task.1 Loading the JSON into Spark Dataframe 

I have done this many times before, but again, there are many ways of howto do it, like:

    val jsonDF = sqlContext.read.json("/tests/json");

    jsonDF.show();

    val json2DF = sqlContext.read.format("json").load("/tests/json");

    json2DF.show();

Zeppelin output:

    jsonDF: org.apache.spark.sql.DataFrame = [age: string, country: string, name: string]
    +---+-------+-----+
    |age|country| name|
    +---+-------+-----+
    | 28|     UK| John|
    | 30|    AUS|Cathy|
    | 50|    USA| Mark|
    +---+-------+-----+
    json2DF: org.apache.spark.sql.DataFrame = [age: string, country: string, name: string]
    +---+-------+-----+
    |age|country| name|
    +---+-------+-----+
    | 28|     UK| John|
    | 30|    AUS|Cathy|
    | 50|    USA| Mark|
    +---+-------+-----+

## Task 2. Filtering the JSON dataframe

Let's display only people having age over 30.

    val peopleJsonDF = sqlContext.read.format("json").load("/tests/json");

    peopleJsonDF.registerTempTable("jsonPeople");

    sqlContext.sql("select * from jsonPeople where age > 30").show();

Zeppelin output:

    peopleJsonDF: org.apache.spark.sql.DataFrame = [age: string, country: string, name: string]
    +---+-------+----+
    |age|country|name|
    +---+-------+----+
    | 50|    USA|Mark|
    +---+-------+----+

## Task 3. Ordering the JSON dataframe

Let's display all the people ordered by age in the descending order.

    val peopleJsonDF = sqlContext.read.format("json").load("/tests/json");

    peopleJsonDF.registerTempTable("jsonPeople");

    sqlContext.sql(" select * from jsonPeople ORDER BY age DESC ").show();

Zeppelin output:

    peopleJsonDF: org.apache.spark.sql.DataFrame = [age: string, country: string, name: string]
    +---+-------+-----+
    |age|country| name|
    +---+-------+-----+
    | 50|    USA| Mark|
    | 30|    AUS|Cathy|
    | 28|     UK| John|
    +---+-------+-----+

## Task 4. Convert JSON dataframe to CSV HDFS file

Task: Convert JSON dataframe to HDFS csv file with fields separated by "|" character.

If they won't allow us during the exam the databricks library then I will solve this with simple RDD map:

    val peopleJsonDF = sqlContext.read.format("json").load("/tests/json");

    peopleJsonDF.registerTempTable("jsonPeople");

    val orderedJsonDF = sqlContext.sql(" select * from jsonPeople ORDER BY age DESC ");

    val resultRDD = orderedJsonDF.rdd.map(row=>row(0)+"|"+row(1)+"|"+row(2));

    resultRDD.saveAsTextFile("/tests/csvres/result");

Zeppelin output first:

    peopleJsonDF: org.apache.spark.sql.DataFrame = [age: string, country: string, name: string]
    orderedJsonDF: org.apache.spark.sql.DataFrame = [age: string, country: string, name: string]
    resultRDD: org.apache.spark.rdd.RDD[String] = MapPartitionsRDD[99] at map at <console>:29

HDFS output:

    [root@sandbox ~]# hdfs dfs -getmerge /tests/csvres/result result.txt
    [root@sandbox ~]# cat result.txt
    50|USA|Mark
    30|AUS|Cathy
    28|UK|John

Best regards

Tomas

